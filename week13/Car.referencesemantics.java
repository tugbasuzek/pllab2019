public class HelloWorld
{
  // arguments are passed using the text field below this editor
  public static void main(String[] args)
  {
    Car myCar=new Car();
    int a=myCar.tires();
    Bus myBus=new Bus();
    int b=myBus.tires();
    myCar=myBus;
    int c=myCar.tires();
    System.out.println(a+" "+b+" "+c);
  }
}

// you can add other public classes to this editor in any order
public class Car
{
  public int tires()
  {
    return 4;
  }
}
public class Bus extends Car
{
  public int tires()
  {
    return 8;
  }
}